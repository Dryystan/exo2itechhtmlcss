let button = document.getElementById("move");

// This handler will be executed only once when the cursor
// moves over the unordered list
button.addEventListener("mouseenter", function( event ) {
  button.style.position = "absolute";
  button.style.left = Math.floor(Math.random() * window.innerWidth)+'px';
  button.style.top = Math.floor(Math.random() * window.innerHeight)+'px';
}, false);
